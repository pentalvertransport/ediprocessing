﻿using Apache.NMS.ActiveMQ;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace ARGOSProcessor
{
    public class ARGOS_XML_Processor
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public bool processData(string text, string PDSDestination, string fileName)
        {
            Consignments consignments = null;
            MemoryStream memStream = null;
            XmlSerializer serializer = null;

            var validTOPSLocations = new List<string>();
            validTOPSLocations.Add("PENTFXT");
            validTOPSLocations.Add("PENTLGW");
            validTOPSLocations.Add("PENTSOU");

            try
            {
                serializer = new XmlSerializer(typeof(Consignments));
                memStream = new MemoryStream(Encoding.UTF8.GetBytes(text));
                consignments = (Consignments)serializer.Deserialize(memStream);

            }
            catch (Exception ex)
            {
                return false;
            }
            LearnEFEntities db = null;
            string fromLocation = null;
            string toLocation = null;
            string fromLocationPostCode = null;
            string toLocationPostCode = null;


            logger.Debug("PDS Destination Database =>" + PDSDestination);
            if (PDSDestination == "FXS")
            {
                fromLocation = "TTY";
                toLocation = "PENTBLO";
                fromLocationPostCode = "IP11 8SH";
                toLocationPostCode = "IP11 4XQ";
                db = new LearnEFEntities("name=LearnEFEntities_FXS");
            }
            else if (PDSDestination == "SOU")
            {
                fromLocation = "SOU - SCT";
                fromLocationPostCode = "SO15 0GN";
                toLocation = "SOU - DG10";

                db = new LearnEFEntities("name=LearnEFEntities_SOU");
            }
            else if (PDSDestination == "LGP")
            {
                fromLocation = "LGP - QUAY";
                toLocation = "LGP - PENT";
                db = new LearnEFEntities("name=LearnEFEntities_LGP");
            }

            for (int x = 0; x < consignments.Consignment.Length; x++)
            {
                logger.Debug("Processing Data For [" + consignments.Consignment[x].Branch + "]");

                if (!validTOPSLocations.Contains(consignments.Consignment[x].Branch))
                {
                    logger.Debug("**************");
                    logger.Debug("Not Processing Branch =>" + consignments.Consignment[x].Branch);
                    logger.Debug("**************");
                    break;
                }

                string container = consignments.Consignment[x].Job.ContainerNumber;
                string sealNumber = consignments.Consignment[x].Job.PackageSealNumbers;

                string outboundPIN = container.Substring(0, 4) + sealNumber.Substring(sealNumber.Trim().Length - 4);
                DateTime lastFreeDate = Convert.ToDateTime(consignments.Consignment[0].Job.LastFreeQuayDate);
                
                string lastFreeQuayDate = String.Format("{0:dd/MM/yyyy}", lastFreeDate);
                logger.Debug("Converted Date =>" + lastFreeQuayDate);
                Shunt objShunt = new Shunt();

                objShunt.PinNumber = outboundPIN;

                objShunt.Comments = "SHUNT BY " + lastFreeQuayDate;
                objShunt.EquipmentNumber = consignments.Consignment[x].Job.ContainerNumber;
                objShunt.ISOCode = consignments.Consignment[x].Job.ContainerType;
                objShunt.CustomerReference = consignments.Consignment[x].Job.CustomerReference;
                objShunt.Vessel = consignments.Consignment[x].Job.VoyageVessel;
                objShunt.Voyage = consignments.Consignment[x].Job.VoyageNumber;
                objShunt.Weight = Convert.ToString(consignments.Consignment[x].Job.GrossWeight.value);
                objShunt.InvoiceReference = consignments.Consignment[x].Job.Waypoint[0].Reference;
                objShunt.Created = DateTime.Now;
                objShunt.CustomerCode = "ARGOS";
                objShunt.FromLocationCode = fromLocation;
                objShunt.FromLocationPostcode = fromLocationPostCode;
                objShunt.ToLocation = toLocation;
                objShunt.ToLocationPostcode = toLocationPostCode;

                db.Shunts.Add(objShunt);
                logger.Debug("Adding Container ...." + objShunt.EquipmentNumber);
            }

                try
                {
                //logger.Debug("Writing to PDS Destination Database =>" + PDSDestination + "[" + db.ShuntsobjShunt.EquipmentNumber);
                logger.Debug("Saving Changes");
             
                db.SaveChanges();

                makeReportRequest(text, fileName);


                return true;

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                    return false;
                }
            finally
            {
                db.Database.Connection.Close();
               
            }
            
        }

        void makeReportRequest(string text, string fileName)
        {   
            try
            {

                string brokerURI = Properties.Settings.Default.BROKER_URI;
                string queueName = Properties.Settings.Default.QUEUE_NAME;

                QueueConnectionFactory factory = new QueueConnectionFactory(new ConnectionFactory(brokerURI));

                using (QueueConnection connection = factory.CreateTransactedConnection(queueName))
                {
                    using (SimpleQueuePublisher publisher = connection.CreateSimpleQueuePublisher())
                    {                       
                        publisher.SendMessage(text);
                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //Exit();
            }

        }









    }

    public partial class LearnEFEntities : DbContext
    {
        public LearnEFEntities(String connString)
            : base(connString)
        {
        }
    }






}
