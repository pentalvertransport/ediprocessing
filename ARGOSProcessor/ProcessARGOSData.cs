﻿using ARGOSProcessor;
using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ARGOSProcessor
{
    class ProcessARGOSData
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected static AutoResetEvent semaphore = new AutoResetEvent(false);
        protected static TimeSpan receiveTimeout = TimeSpan.FromSeconds(10);
        protected static ITextMessage message = null;

        //const string BROKER_URI = "tcp://localhost:61616";
        //const string QUEUE_NAME = "PDS.ARGOS";

        static void Main(string[] args)
        {
            try
            {

                string brokerURI = Properties.Settings.Default.BROKER_URI;
                string queueName = Properties.Settings.Default.QUEUE_NAME;

                QueueConnectionFactory factory = new QueueConnectionFactory(new ConnectionFactory(brokerURI));

                using (QueueConnection connection = factory.CreateTransactedConnection(queueName))
                {
                    using (SimpleQueueListener listener = connection.CreateSimpleQueueListener(new MessageProcessor(true)))
                    {
                        Exit();
                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Exit();
            }
        }

        static void Exit()
        {
            Console.WriteLine("Waiting for messages .....Press any key to exit...");
            Console.ReadKey();
        }
        
    }
   
    }
