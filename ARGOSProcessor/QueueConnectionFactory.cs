﻿using Apache.NMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGOSProcessor
{
    public class QueueConnectionFactory
    {
        private readonly IConnectionFactory connectionFactory;

        public QueueConnectionFactory(IConnectionFactory connectionFactory)
        {
            this.connectionFactory = connectionFactory;
        }

        public QueueConnection CreateConnection(string queueName)
        {
            return new QueueConnection(this.connectionFactory, queueName);
        }

        public QueueConnection CreateTransactedConnection(string queueName)
        {
            return new QueueConnection(this.connectionFactory, queueName, AcknowledgementMode.Transactional);
        }
    }
}
