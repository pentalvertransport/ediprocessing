﻿using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.Policies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARGOSProcessor
{
    public class SimpleQueueListener : IDisposable
    {
        private readonly IMessageConsumer consumer;
        private bool isDisposed = false;
        private readonly IMessageProcessor processor;
        private readonly ISession session;

        public SimpleQueueListener(IMessageConsumer consumer, IMessageProcessor processor, ISession session)
        {
            this.consumer = consumer;
            MessageConsumer activeMqConsumer = this.consumer as MessageConsumer;
            if (activeMqConsumer != null)
            {
                RedeliveryPolicy redeliveryPolicy = new RedeliveryPolicy();
                redeliveryPolicy.MaximumRedeliveries = -1;
                redeliveryPolicy.RedeliveryDelay(1000);
                activeMqConsumer.RedeliveryPolicy = redeliveryPolicy;
            }
            this.consumer.Listener += new MessageListener(OnMessage);
            this.processor = processor;
            this.session = session;
          
        }

        public void OnMessage(IMessage message)
        {
            ITextMessage textMessage = message as ITextMessage;
            if (this.processor.ReceiveMessage(textMessage))
            {
                Console.WriteLine("Process my argos xml");
                ARGOS_XML_Processor xmlp = new ARGOS_XML_Processor();
                if (xmlp.processData(textMessage.Text, "FXS", message.Properties["FileName"].ToString()))
                {
                    this.session.Commit();
                }
                else
                {
                    this.session.Rollback();
                }
            }
            else
            {
                Console.WriteLine("Error - returning message to queue.");
                this.session.Rollback();
            }
        }
        #region IDisposable Members

        public void Dispose()
        {
            if (!this.isDisposed)
            {
                this.consumer.Dispose();
                this.isDisposed = true;
            }
        }

        #endregion
    }
}
