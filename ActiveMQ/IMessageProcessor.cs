﻿using Apache.NMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveMQ
{
    public interface IMessageProcessor
    {
        bool ReceiveMessage(ITextMessage message);
    }
}
