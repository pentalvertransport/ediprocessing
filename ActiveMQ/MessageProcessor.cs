﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apache.NMS;

namespace ActiveMQ
{
    public class MessageProcessor : IMessageProcessor
    {
        private bool errorOnNextMessage = false;
        private int numberOfErrors;
        private int errorCount = 0;

        public MessageProcessor() : this(false)
        {
        }

        public MessageProcessor(bool errorOnNextMessage) : this(errorOnNextMessage, 10)
        {
        }

        public MessageProcessor(bool errorOnNextMessage, int numberOfErrors)
        {
            this.errorOnNextMessage = errorOnNextMessage;
            this.numberOfErrors = numberOfErrors;
        }

        internal bool ErrorOnNextMessage
        {
            set
            {
                this.errorOnNextMessage = value;
            }
        }

        #region IMessageProcessor Members

     //   public bool ReceiveMessage(Apache.NMS.ITextMessage message)
     //   {
     //       if (message.NMSRedelivered)
     //       {
     //           Console.WriteLine("The following message is being redelivered:");
     //       }
     //       Console.WriteLine(message.Text);
     //       bool result = !this.errorOnNextMessage;
     //       if (this.errorOnNextMessage)
     //       {
     //           this.errorCount++;
     //           this.errorOnNextMessage = (errorCount <= numberOfErrors);
     //       }
     //       return result;

     //   }
        public bool ReceiveMessage(Apache.NMS.ITextMessage message)
        {
            Console.WriteLine("Receiev Message" + message.NMSMessageId);
            return true;

        }

        // public bool ReceiveMessage(ITextMessage message)
        // {
        //     throw new NotImplementedException();
        // }

        #endregion
    }
}
