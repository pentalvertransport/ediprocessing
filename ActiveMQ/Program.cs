﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.Util;
using System.Threading;
using Apache.NMS.ActiveMQ;

namespace ActiveMQ
{
    class Program
    {
        const string BROKER_URI = "tcp://localhost:61616";
        const string QUEUE_NAME = "test.queue";

        const string FORMAT = "Message {0}";

        static void Main(string[] args)
        {
            try
            {
                QueueConnectionFactory factory = new QueueConnectionFactory(new ConnectionFactory(BROKER_URI));

                for (int i = 1; i < 11; i++)
                {
                    using (QueueConnection connection = factory.CreateConnection(QUEUE_NAME))
                    {
                        using (SimpleQueuePublisher publisher = connection.CreateSimpleQueuePublisher())
                        {
                            publisher.SendMessage(string.Format(FORMAT, i));
                            Console.WriteLine(i);
                        }
                    }
                }
                Exit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Exit();
            }
        }

        static void Exit()
        {
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
