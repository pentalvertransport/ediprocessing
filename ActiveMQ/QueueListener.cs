﻿using Apache.NMS.ActiveMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveMQ

{
    class QueueListener
    {
        const string BROKER_URI = "tcp://localhost:61616";
        const string QUEUE_NAME = "test.queue";

        static void Main(string[] args)
        {
            try
            {
                QueueConnectionFactory factory = new QueueConnectionFactory(new ConnectionFactory(BROKER_URI));

                using (QueueConnection connection = factory.CreateTransactedConnection(QUEUE_NAME))
                {
                    using (SimpleQueueListener listener = connection.CreateSimpleQueueListener(new MessageProcessor(true)))
                    {
                        Exit();
                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Exit();
            }
        }

            static void Exit()
            {
                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
    }
}
