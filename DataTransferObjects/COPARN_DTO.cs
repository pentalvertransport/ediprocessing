﻿using EDIUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace uk.co.pentalver.pds.DataTransferObjects
{
    public class COPARN_DTO : DTO
    {
        private string messageReferenceNumber;
        private SerializableDictionary<string, string> customerReferences;
        private string containerISOCode;
        private string containerNumber;
        private string containerSequence;
        private string grossWeight;
        private string tareWeight;
        private string expiryDateTime;
        private string deliveryDateTime;
        private SerializableDictionary<string, string> freeText;
        private string emptyFull;

        private string payload;

        public string MessageReferenceNumber
        {
            get
            {
                return messageReferenceNumber;
            }

            set
            {
                messageReferenceNumber = value;
            }
        }
        

        public string ContainerISOCode
        {
            get
            {
                return containerISOCode;
            }

            set
            {
                containerISOCode = value;
            }
        }

        public string ContainerNumber
        {
            get
            {
                return containerNumber;
            }

            set
            {
                containerNumber = value;
            }
        }

        public string ContainerSequence
        {
            get
            {
                return containerSequence;
            }

            set
            {
                containerSequence = value;
            }
        }

        public string GrossWeight
        {
            get
            {
                return grossWeight;
            }

            set
            {
                grossWeight = value;
            }
        }

        public string TareWeight
        {
            get
            {
                return tareWeight;
            }

            set
            {
                tareWeight = value;
            }
        }

        public string ExpiryDateTime
        {
            get
            {
                return expiryDateTime;
            }

            set
            {
                expiryDateTime = value;
            }
        }

        public string DeliveryDateTime
        {
            get
            {
                return deliveryDateTime;
            }

            set
            {
                deliveryDateTime = value;
            }
        }

        public string Payload
        {
            get
            {
                return payload;
            }

            set
            {
                payload = value;
            }
        }
        
        public SerializableDictionary<string, string> FreeText
        {
            get
            {
                return freeText;
            }

            set
            {
                freeText = value;
            }
        }

        public string getFreeText()
        {
            String txt = null;

            foreach(var item in freeText.Values)
            {
                txt = txt + item;
            }            

            return txt;

        }

       

        public SerializableDictionary<string, string> CustomerReferences
        {
            get
            {
                return customerReferences;
            }

            set
            {
                customerReferences = value;
            }
        }

        public string EmptyFull
        {
            get
            {
                return emptyFull;
            }

            set
            {
                emptyFull = value;
            }
        }
    }
}
