﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace uk.co.pentalver.pds.DataTransferObjects
{
    public class DemoDTO : DTO
    {
        private string demoId = "";
        private string demoName = "";
        private string demoProgrammer = "";

        [XmlElement(IsNullable = true)]
        public string DemoId
        {
            get
            {
                return demoId;
            }

            set
            {
                demoId = value;
            }
        }

        [XmlElement(IsNullable = true)]
        public string DemoName
        {
            get
            {
                return demoName;
            }

            set
            {
                demoName = value;
            }
        }

        [XmlElement(IsNullable = true)]
        public string DemoProgrammer
        {
            get
            {
                return demoProgrammer;
            }

            set
            {
                demoProgrammer = value;
            }
        }
    }
}
