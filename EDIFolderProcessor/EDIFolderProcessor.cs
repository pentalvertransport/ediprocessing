﻿using Apache.NMS;
using Apache.NMS.Util;
using FluentFTP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EDIFolderProcessor
{
    class EDIFolderProcessor
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            logger.Debug("Starting");

            string folderToWatch = "";

            if (args.Length >0)
            {
                folderToWatch = args[0];                
            }
            else
            {
                folderToWatch = "c:/temp/hapag";
            }

            while (true)
            {
                System.Threading.Thread.Sleep(5000);
                logger.Info("Waking up ....");
                // this may be needed to pick up stray files
                //processFilesInFolder(folderToWatch);
                processFTPFolder("/Argos/xml");

            }
        }


        private static void processFilesInFolder(string folder)
        {
            logger.Debug("Processing " + folder);
            DirectoryInfo d = new DirectoryInfo(folder);
            FileInfo[] Files = d.GetFiles("*.*");             
            foreach (FileInfo file in Files)
            {
                processFile(file.Name, file.FullName);
            }
        }


        private static void processFTPFolder(string folderName)
        {
            FtpClient client = new FtpClient(Properties.Settings.Default.FTPHost);
            
            client.Credentials = new NetworkCredential(Properties.Settings.Default.FTPUser, Properties.Settings.Default.FTPPassword);
            logger.Debug("Connecting to FTP server =>" + Properties.Settings.Default.FTPHost);
            client.Connect();
            logger.Debug("Connected");
            foreach (FtpListItem item in client.GetListing(folderName))
            {
                // if this is a file
                if (item.Type == FtpFileSystemObjectType.File)
                {
                    logger.Debug("Downloading File =>" + item.FullName);
                    client.DownloadFile(@"C:\temp\" + item.FullName, item.FullName);
                    logger.Debug("Downloaded, moving to archive =>" + item.FullName);
                    client.MoveFile(item.FullName, folderName + "/archive/" + item.Name);

                    logger.Debug("Processing file ..." + item.FullName);
                    processFile(item.Name, @"c:/temp/" + item.FullName);
                    logger.Debug("Processed file ..." + item.FullName);
                    
                }
            }
            client.Disconnect();
        }


        private static string findQueue(string filename)
        {
            string queueName = null;

            if(filename.StartsWith("5"))
            {               
                queueName = "SOU";
            }
            else if(filename.StartsWith("17"))
            {
                queueName = "FXS";
            }
            else if(filename.StartsWith("18"))
            {
                queueName = "LGP";
            }
            else
            {
                queueName = "XXX";
            }
            logger.Debug("Queue = " + queueName);

            return queueName;
            
        }


       
        private static void processFile(string filename, string fullPath)
        {
            String fileContents = File.ReadAllText(fullPath);

            string messageDestination = findQueue(filename);

            // logger.Debug(fileContents.ToString());

            //Uri connecturi = new Uri("activemq:tcp://127.0.0.1:61616");
            Uri connecturi = new Uri(Properties.Settings.Default.ActiveMQHost);
            IConnectionFactory factory = new NMSConnectionFactory(connecturi);
            using (IConnection connection = factory.CreateConnection())
            using (ISession session = connection.CreateSession())
            {
                IDestination destination = SessionUtil.GetDestination(session, "PDS.ARGOS");
                logger.Debug("Using destination: " + destination);

                // Create a consumer and producer                
                using (IMessageProducer producer = session.CreateProducer(destination))
                {
                    connection.Start();
                    producer.DeliveryMode = MsgDeliveryMode.Persistent;

                    // Send a message
                    ITextMessage request = session.CreateTextMessage(fileContents);
                    request.NMSCorrelationID = "abc";
                    request.Properties["PDS"] = messageDestination;
                    request.Properties["FileName"] = filename;

                    try
                    {
                        producer.Send(request);
                        logger.Debug("Written to queue");
                        // if sent delete file
                        File.Delete(fullPath);
                        logger.Debug("File Deleted");
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    
                }
            }







        }


    }
}
