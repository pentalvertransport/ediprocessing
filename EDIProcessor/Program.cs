﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace EDIProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World");
            watchFolder("c:/temp/hapag");

            while(true)
            {
                System.Threading.Thread.Sleep(5000);
                Console.WriteLine("Slept");
            }
        }



        private static void watchFolder(string path)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = path;
            watcher.NotifyFilter = NotifyFilters.LastWrite;
            watcher.Filter = "*.*";
            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.EnableRaisingEvents = true;
        }

        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            //Copies file to another directory.
            Console.WriteLine(e.Name);
        }
    }
}
