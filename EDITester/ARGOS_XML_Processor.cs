﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace uk.co.pentalver.www.pds.edi.EDITester
{
    public class ARGOS_XML_Processor
    {
       public void processData(string text)
        {
        Console.WriteLine("From the EDI Classes --->" + text);
     
        XmlSerializer serializer = new XmlSerializer(typeof(Consignments));
        MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(text));
        Consignments consignments = (Consignments)serializer.Deserialize(memStream);

        Console.WriteLine(consignments.Consignment[0].Job.ContainerNumber);
        Console.WriteLine(consignments.Consignment[0].Job.ContainerType);
        Console.WriteLine(consignments.Consignment[0].Job.CustomerReference);
        Console.WriteLine(consignments.Consignment[0].Job.VoyageVessel);
        Console.WriteLine(consignments.Consignment[0].Job.VoyageNumber);
        Console.WriteLine(consignments.Consignment[0].Job.VoyageVessel);
        Console.WriteLine(consignments.Consignment[0].Job.PackageSealNumbers);
        Console.WriteLine(consignments.Consignment[0].Job.GrossWeight.value);
        Console.WriteLine(consignments.Consignment[0].Job.NetWeight.value);
        Console.WriteLine(consignments.Consignment[0].Job.SCACCode);
        Console.WriteLine(consignments.Consignment[0].Job.Waypoint[0].Date);
        Console.WriteLine(consignments.Consignment[0].Job.Waypoint[0].Reference);
        Console.WriteLine(consignments.Consignment[0].Job.Waypoint[0].PortCode);
        Console.WriteLine(consignments.Consignment[0].Job.Waypoint[1].DepotCode);
        }





    }
}
