﻿using EdiFabric.Rules.EdifactD95BCOREOR;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uk.co.pentalver.www.pds.edi.EDITester
{
    
    public class COREORProcessorBase_CMA
    {
        public int numberOfUnits;
        public string sender;
        public string receiver;
        public string customerReference;
        public M_COREOR messageToProcess = null;
        public string specialInstructions;
        public List<EquipmentDetails> equipmentDetails;

        private static readonly ILog logger =
           LogManager.GetLogger(typeof(COREORProcessorBase_CMA));


        public void parse(object message,string sender, string receiver)
        {
            this.sender = sender;
            this.receiver = receiver;

            messageToProcess = (M_COREOR)message;

            logger.Debug("Getting Container Details");
            getContainerDetails();
            logger.Debug("Getting Customer Reference");
            customerReference = getCustomerReference();
            logger.Debug("Getting Special Instructions");
            specialInstructions = getSpecialInstructions();

            Console.WriteLine(numberOfUnits);
            Console.WriteLine(customerReference);
            
        }

        public virtual void getContainerDetails()
        {
            string container;
            StringBuilder si = new StringBuilder();
            equipmentDetails = new List<EquipmentDetails>();
            numberOfUnits = messageToProcess.G_EQD.Count;
            
            for (int i = 0; i < messageToProcess.G_EQD.Count; i++)
            {
                for (int x = 0; x < messageToProcess.G_EQD[i].S_FTX_4.Count; x++)
                {
                    si.Append(messageToProcess.G_EQD[i].S_FTX_4[x].C_C108_4.D_4440_1);
                }
                if (null != messageToProcess.G_EQD[i].S_EQD.C_C237_2)
                {
                    container = messageToProcess.G_EQD[i].S_EQD.C_C237_2.D_8260_1;
                }
                else
                {
                    container = "";
                }                    
                equipmentDetails.Add(new EquipmentDetails(messageToProcess.G_EQD[i].S_EQD.C_C224.D_8155_1, container, messageToProcess.G_EQD[i].S_EQD.D_8169_6));
            }            
        }

        public virtual string getSpecialInstructions()
        {
            string specialInstructions = null;         

            return specialInstructions;
        }

        public virtual string getCustomerReference()
        {
            return messageToProcess.S_BGM.D_1004_2; 
        }
    }
}
