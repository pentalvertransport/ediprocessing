﻿using log4net;
using log4net.Repository.Hierarchy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uk.co.pentalver.www.pds.edi.EDITester
{
    public class COREORProcessor_CMA : COREORProcessorBase_CMA
    {
        private static readonly ILog logger =
          LogManager.GetLogger(typeof(COREORProcessorBase_CMA));

        public override string getCustomerReference()
        {
            logger.Debug("CMA");
            return messageToProcess.S_RFF[0].C_C506.D_1154_2;            
        }


    }
}
