﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uk.co.pentalver.www.pds.edi.EDITester
{

    public class EquipmentDetails
    {
        string isoCode;
        string containerNumber;
        string loadedEmpty;

        public string IsoCode
        {
            get
            {
                return isoCode;
            }

            set
            {
                isoCode = value;
            }
        }

        public string ContainerNumber
        {
            get
            {
                return containerNumber;
            }

            set
            {
                containerNumber = value;
            }
        }

        public string LoadedEmpty
        {
            get
            {
                return loadedEmpty;
            }

            set
            {
                loadedEmpty = value;
            }
        }

        public EquipmentDetails(string isoCode, string containerNumber, string loadedEmpty)
        {
            this.isoCode = isoCode;
            this.containerNumber = containerNumber;
            this.loadedEmpty = loadedEmpty;
        }
    }
}
