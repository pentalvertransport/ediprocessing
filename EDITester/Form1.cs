﻿using System;
using System.Text;
using EdiFabric.Framework.Controls;
using EdiFabric.Framework.Exceptions;
using EdiFabric.Framework.Readers;
using EdiFabric.Rules.EdifactD95BCOREOR;
using System.IO;
using System.Windows.Forms;
using uk.co.pentalver.www.pds.edi.EDITester;
using log4net;
using log4net.Config;
using System.Xml.Serialization;
using EdiFabric.Rules.EdifactD95BCODECO;
using EdiFabric.Framework;
using System.Collections.Generic;

namespace EDITester
{
    public partial class Form1 : Form
    {
        const string EMPTY = "4";
        const string FULL = "5";

        private static readonly ILog logger =
           LogManager.GetLogger(typeof(Form1));

     
       
           

        public Form1()
        {
            InitializeComponent();
            BasicConfigurator.Configure();
        }



        private void ParseEDI()
        {
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(textBoxSourceEDI.Text));

            String sender = null;
            string receiver = null;
            
            var ediReader = EdifactReader.Create(ms);
            
           // logger.Debug(textBoxSourceEDI.Text);
            using (ediReader)
            {
                while (ediReader.Read())
                {
                    logger.Debug("Hello");
                    object currentItem = ediReader.Item;
                    if (currentItem is IEdiControl)
                    {                        
                        if(currentItem is S_UNB)
                        {
                            logger.Debug("Processing UNB Segment");
                            S_UNB unb = (S_UNB)currentItem;
                            sender = unb.C_S002.D_0004_1;
                            receiver = unb.C_S003.D_0010_1;
                            logger.Debug("Sender - > " + sender);
                        }                
                        else if(currentItem is S_UNZ)
                        {
                            logger.Debug("Processing UNZ Segment");
                        }
                        continue;
                    }
                    var exception = currentItem as ParsingException;
                    if (exception != null)
                    {
                        logger.Debug("Parsing Exception [" + exception);
                        // do something with the exception
                        continue;
                    }

                    // do something with the current item if it's of interest, e.g. save to database, etc.
                    if (currentItem is EdiFabric.Rules.EdifactD00BCOPARN.M_COPARN)
                    {
                        processCOPARND95B(currentItem);
                    }
                    else if (currentItem is EdiFabric.Rules.EdifactD95BCOPARN.M_COPARN)
                    {
                        processCOPARND95B(currentItem);
                    }
                    else if (currentItem is M_COREOR)
                    {
                        processCOREOR(currentItem, sender, receiver);
                    }
                    else if (currentItem is M_CODECO)
                    {
                        XmlSerializer ser = new XmlSerializer(typeof(M_CODECO));
                        StringWriter stringWriter = new StringWriter();
                        ser.Serialize(stringWriter, currentItem);
                        string serializedXML = stringWriter.ToString();
                        Console.WriteLine("CODECO");
                        

                    }
                    else
                    {

                    }
               }
                
            }
        }

        private void processCOPARND95B(object currentItem)
        {
            var message = (EdiFabric.Rules.EdifactD00BCOPARN.M_COPARN)currentItem;

            textBox4.Text = "COPARN";

            // validate
            //var errors = EdiValidation.Validate(message).Flatten().ToList();
            //if(errors.Any())
            //{
            //    foreach (var error in errors)
            //    {

            //    }
            //}
        }

        private string translateSender(string sender)
        {
            // this translates senderid into our client codes
            if(sender == "CMA-CGM")
            {
                return "CMA";
            }
            else
            {
                return sender;
            }
        }
        private void processCOREOR(object currentItem,string sender, string receiver)
        {
            COREORProcessorBase_CMA coreorProcessor = null;

            var message = (M_COREOR)currentItem;
                        
            string senderID = translateSender(sender);
            if(senderID == "CMA")
            {
                coreorProcessor = new COREORProcessor_CMA();
            }
            else
            {
                coreorProcessor = new COREORProcessorBase_CMA();
            }

            
            coreorProcessor.parse(message,sender,receiver);
            
            // validate
            //var errors = EdiValidation.Validate(message).Flatten().ToList();
            //if (errors.Any())
            //{
            //    foreach (var error in errors)
            //    {

            //    }
            //}
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ParseEDI();            
        }






        private void button4_Click(object sender, EventArgs e)
        {
            ARGOSXML(textBoxSourceEDI.Text);


            //Console.WriteLine(consignments.Consignment[0].Job.);



            Console.WriteLine("Hello");



        }

        private void ARGOSXML(string data)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(Consignments));
            MemoryStream memStream = new MemoryStream(Encoding.UTF8.GetBytes(data));
            Consignments consignments = (Consignments)serializer.Deserialize(memStream);

            Console.WriteLine(consignments.Consignment[0].Job.ContainerNumber);
            Console.WriteLine(consignments.Consignment[0].Job.ContainerType);
            Console.WriteLine(consignments.Consignment[0].Job.CustomerReference);
            Console.WriteLine(consignments.Consignment[0].Job.VoyageVessel);
            Console.WriteLine(consignments.Consignment[0].Job.VoyageNumber);
            Console.WriteLine(consignments.Consignment[0].Job.VoyageVessel);
            Console.WriteLine(consignments.Consignment[0].Job.PackageSealNumbers);
            Console.WriteLine(consignments.Consignment[0].Job.GrossWeight.value);
            Console.WriteLine(consignments.Consignment[0].Job.NetWeight.value);
            Console.WriteLine(consignments.Consignment[0].Job.SCACCode);
            Console.WriteLine(consignments.Consignment[0].Job.Waypoint[0].Date);
            Console.WriteLine(consignments.Consignment[0].Job.Waypoint[0].Reference);
            Console.WriteLine(consignments.Consignment[0].Job.Waypoint[0].PortCode);
            Console.WriteLine(consignments.Consignment[0].Job.Waypoint[1].DepotCode);
        }

        private void pbCreateMessage_Click(object sender, EventArgs e)
        {
            M_CODECO codeco = new M_CODECO();

            S_UNB interchangeHeader = new S_UNB();

            interchangeHeader.C_S002 = new C_S002();
            interchangeHeader.C_S002.D_0004_1 = "SENDER";
            interchangeHeader.C_S003 = new C_S003();
            interchangeHeader.C_S003.D_0010_1 = "RECEIVER";
            
            EdiFabric.Rules.EdifactD95BCODECO.S_UNH unh = new EdiFabric.Rules.EdifactD95BCODECO.S_UNH();
            unh.C_S009 = new EdiFabric.Rules.EdifactD95BCODECO.C_S009();
            unh.C_S009.D_0051_4 = "1";
            unh.D_0062_1 = "901";

            codeco.S_UNH = unh;

            var ediGroup = new EdifactGroup<M_CODECO>(null);

            ediGroup.AddItem(codeco);

            var ediInterchange = new EdifactInterchange(interchangeHeader);
            ediInterchange.AddItem(ediGroup);

            var defaultSeperator = Separators.DefaultEdifact();

            IEnumerable<string> ediSegments = ediInterchange.GenerateEdi();
            

            foreach (var segment in ediSegments)
            {
                textBoxSourceEDI.AppendText(segment + "\r\n");
                Console.WriteLine(segment);
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var db = new PetesPDSEntities1();

            PreAdvice row = new PreAdvice();
            row.CustomerReference = "test";
            //row.Comments = contents + specialInstructions;
            //row.NUMBEROFUNITS = quantity;
            //row.Weight = weight;

            db.PreAdvices.Add(row);
            db.SaveChanges();


        }
    }
}
