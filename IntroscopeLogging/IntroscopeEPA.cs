﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IntroscopeLogging
{
    public class IntroscopeEPA
    {



        private static void LogEPA(int unreadMessages, string mailbox, long elapsedMs)
        {
            string epaURL = null;
            string result = null;
            string epaAddress = null;

            String hostName = Dns.GetHostName();

            epaAddress = hostName + ":9090";

            using (var client = new WebClient())
            {
                try
                {
                    epaURL = "http://" + epaAddress + "/?metricType=StringEvent&metricName=ExchangeTest|" + mailbox + ":Name" + "&metricValue=" + mailbox;
                    // Console.WriteLine(epaURL);
                    result = client.DownloadString(epaURL);
                    //Console.WriteLine(result);
                    epaURL = "http://" + epaAddress + "/?metricType=IntCounter&metricName=ExchangeTest|" + mailbox + ":UnreadCount" + "&metricValue=" + unreadMessages;
                    //Console.WriteLine(epaURL);
                    result = client.DownloadString(epaURL);
                    //Console.WriteLine(result);
                    epaURL = "http://" + epaAddress + "/?metricType=IntCounter&metricName=ExchangeTest|" + mailbox + ":ResponseTime" + "&metricValue=" + elapsedMs;
                    Console.WriteLine(epaURL);
                    result = client.DownloadString(epaURL);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception posting....");
                    Console.WriteLine(epaURL);
                }
            }
        }

    }


   



}
