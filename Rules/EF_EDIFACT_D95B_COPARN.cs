namespace EdiFabric.Rules.EdifactD95BCOPARN {
    using System;
    using System.Xml.Serialization;
    using System.Collections.Generic;
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class M_COPARN {
    [XmlElement(Order=0)]
    public S_UNH S_UNH {get; set;}
    [XmlElement(Order=1)]
    public S_BGM S_BGM {get; set;}
    [XmlElement(Order=2)]
    public S_TMD S_TMD {get; set;}
    [XmlElement(Order=3)]
    public S_TSR S_TSR {get; set;}
    [XmlElement("S_FTX",Order=4)]
    public List<S_FTX> S_FTX {get; set;}
    [XmlElement("S_RFF",Order=5)]
    public List<S_RFF> S_RFF {get; set;}
    [XmlElement("G_TDT",Order=6)]
    public List<G_TDT> G_TDT {get; set;}
    [XmlElement("G_NAD",Order=7)]
    public List<G_NAD> G_NAD {get; set;}
    [XmlElement("G_GID",Order=8)]
    public List<G_GID> G_GID {get; set;}
    [XmlElement("G_EQD",Order=9)]
    public List<G_EQD> G_EQD {get; set;}
    [XmlElement(Order=10)]
    public S_CNT S_CNT {get; set;}
    [XmlElement(Order=11)]
    public S_UNT S_UNT {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_UNH {
    [XmlElement(Order=0)]
    public string D_0062_1 {get; set;}
    [XmlElement(Order=1)]
    public C_S009 C_S009 {get; set;}
    [XmlElement(Order=2)]
    public string D_0068_3 {get; set;}
    [XmlElement(Order=3)]
    public C_S010 C_S010 {get; set;}
    [XmlElement(Order=4)]
    public C_S016 C_S016 {get; set;}
    [XmlElement(Order=5)]
    public C_S017 C_S017 {get; set;}
    [XmlElement(Order=6)]
    public C_S018 C_S018 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_S009 {
    [XmlElement(Order=0)]
    public string D_0065_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_0052_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_0054_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_0051_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_0057_5 {get; set;}
    [XmlElement(Order=5)]
    public string D_0110_6 {get; set;}
    [XmlElement(Order=6)]
    public string D_0113_7 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_S010 {
    [XmlElement(Order=0)]
    public string D_0070_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_0073_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_S016 {
    [XmlElement(Order=0)]
    public string D_0115_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_0116_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_0118_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_0051_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_S017 {
    [XmlElement(Order=0)]
    public string D_0121_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_0122_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_0124_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_0051_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_S018 {
    [XmlElement(Order=0)]
    public string D_0127_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_0128_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_0130_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_0051_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_BGM {
    [XmlElement(Order=0)]
    public C_C002 C_C002 {get; set;}
    [XmlElement(Order=1)]
    public string D_1004_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1225_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4343_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C002 {
    [XmlElement(Order=0)]
    public string D_1001_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_1000_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_TMD {
    [XmlElement(Order=0)]
    public C_C219 C_C219 {get; set;}
    [XmlElement(Order=1)]
    public string D_8332_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_8341_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C219 {
    [XmlElement(Order=0)]
    public string D_8335_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8334_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_TSR {
    [XmlElement(Order=0)]
    public C_C536 C_C536 {get; set;}
    [XmlElement(Order=1)]
    public C_C233 C_C233 {get; set;}
    [XmlElement(Order=2)]
    public C_C537 C_C537 {get; set;}
    [XmlElement(Order=3)]
    public C_C703 C_C703 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C536 {
    [XmlElement(Order=0)]
    public string D_4065_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C233 {
    [XmlElement(Order=0)]
    public string D_7273_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_7273_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_1131_5 {get; set;}
    [XmlElement(Order=5)]
    public string D_3055_6 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C537 {
    [XmlElement(Order=0)]
    public string D_4219_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C703 {
    [XmlElement(Order=0)]
    public string D_7085_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_FTX {
    [XmlElement(Order=0)]
    public string D_4451_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_4453_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C107 C_C107 {get; set;}
    [XmlElement(Order=3)]
    public C_C108 C_C108 {get; set;}
    [XmlElement(Order=4)]
    public string D_3453_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C107 {
    [XmlElement(Order=0)]
    public string D_4441_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C108 {
    [XmlElement(Order=0)]
    public string D_4440_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_4440_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_4440_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4440_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_4440_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_RFF {
    [XmlElement(Order=0)]
    public C_C506 C_C506 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C506 {
    [XmlElement(Order=0)]
    public string D_1153_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1154_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1156_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4000_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_TDT {
    [XmlElement(Order=0)]
    public S_TDT S_TDT {get; set;}
    [XmlElement("S_RFF_2",Order=1)]
    public List<S_RFF_2> S_RFF_2 {get; set;}
    [XmlElement("S_LOC",Order=2)]
    public List<S_LOC> S_LOC {get; set;}
    [XmlElement("S_DTM",Order=3)]
    public List<S_DTM> S_DTM {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_TDT {
    [XmlElement(Order=0)]
    public string D_8051_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8028_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C220 C_C220 {get; set;}
    [XmlElement(Order=3)]
    public C_C228 C_C228 {get; set;}
    [XmlElement(Order=4)]
    public C_C040 C_C040 {get; set;}
    [XmlElement(Order=5)]
    public string D_8101_6 {get; set;}
    [XmlElement(Order=6)]
    public C_C401 C_C401 {get; set;}
    [XmlElement(Order=7)]
    public C_C222 C_C222 {get; set;}
    [XmlElement(Order=8)]
    public string D_8281_9 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C220 {
    [XmlElement(Order=0)]
    public string D_8067_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8066_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C228 {
    [XmlElement(Order=0)]
    public string D_8179_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8178_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C040 {
    [XmlElement(Order=0)]
    public string D_3127_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3128_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C401 {
    [XmlElement(Order=0)]
    public string D_8457_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8459_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_7130_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C222 {
    [XmlElement(Order=0)]
    public string D_8213_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_8212_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_8453_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_RFF_2 {
    [XmlElement(Order=0)]
    public C_C506_2 C_C506_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C506_2 {
    [XmlElement(Order=0)]
    public string D_1153_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1154_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1156_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4000_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_LOC {
    [XmlElement(Order=0)]
    public string D_3227_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C517 C_C517 {get; set;}
    [XmlElement(Order=2)]
    public C_C519 C_C519 {get; set;}
    [XmlElement(Order=3)]
    public C_C553 C_C553 {get; set;}
    [XmlElement(Order=4)]
    public string D_5479_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C517 {
    [XmlElement(Order=0)]
    public string D_3225_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3224_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C519 {
    [XmlElement(Order=0)]
    public string D_3223_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3222_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C553 {
    [XmlElement(Order=0)]
    public string D_3233_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3232_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DTM {
    [XmlElement(Order=0)]
    public C_C507 C_C507 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C507 {
    [XmlElement(Order=0)]
    public string D_2005_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_2380_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_2379_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_NAD {
    [XmlElement(Order=0)]
    public S_NAD S_NAD {get; set;}
    [XmlElement("S_CTA",Order=1)]
    public List<S_CTA> S_CTA {get; set;}
    [XmlElement("S_RFF_3",Order=2)]
    public List<S_RFF_3> S_RFF_3 {get; set;}
    [XmlElement("S_DTM_2",Order=3)]
    public List<S_DTM_2> S_DTM_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_NAD {
    [XmlElement(Order=0)]
    public string D_3035_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C082 C_C082 {get; set;}
    [XmlElement(Order=2)]
    public C_C058 C_C058 {get; set;}
    [XmlElement(Order=3)]
    public C_C080 C_C080 {get; set;}
    [XmlElement(Order=4)]
    public C_C059 C_C059 {get; set;}
    [XmlElement(Order=5)]
    public string D_3164_6 {get; set;}
    [XmlElement(Order=6)]
    public string D_3229_7 {get; set;}
    [XmlElement(Order=7)]
    public string D_3251_8 {get; set;}
    [XmlElement(Order=8)]
    public string D_3207_9 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C082 {
    [XmlElement(Order=0)]
    public string D_3039_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C058 {
    [XmlElement(Order=0)]
    public string D_3124_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3124_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3124_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3124_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_3124_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C080 {
    [XmlElement(Order=0)]
    public string D_3036_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3036_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3036_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3036_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_3036_5 {get; set;}
    [XmlElement(Order=5)]
    public string D_3045_6 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C059 {
    [XmlElement(Order=0)]
    public string D_3042_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3042_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3042_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_CTA {
    [XmlElement(Order=0)]
    public string D_3139_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C056 C_C056 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C056 {
    [XmlElement(Order=0)]
    public string D_3413_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3412_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_RFF_3 {
    [XmlElement(Order=0)]
    public C_C506_3 C_C506_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C506_3 {
    [XmlElement(Order=0)]
    public string D_1153_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1154_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1156_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4000_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DTM_2 {
    [XmlElement(Order=0)]
    public C_C507_2 C_C507_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C507_2 {
    [XmlElement(Order=0)]
    public string D_2005_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_2380_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_2379_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_GID {
    [XmlElement(Order=0)]
    public S_GID S_GID {get; set;}
    [XmlElement("S_HAN",Order=1)]
    public List<S_HAN> S_HAN {get; set;}
    [XmlElement("S_FTX_2",Order=2)]
    public List<S_FTX_2> S_FTX_2 {get; set;}
    [XmlElement("S_RFF_4",Order=3)]
    public List<S_RFF_4> S_RFF_4 {get; set;}
    [XmlElement("S_PIA",Order=4)]
    public List<S_PIA> S_PIA {get; set;}
    [XmlElement("G_NAD_2",Order=5)]
    public List<G_NAD_2> G_NAD_2 {get; set;}
    [XmlElement("S_MEA",Order=6)]
    public List<S_MEA> S_MEA {get; set;}
    [XmlElement("S_DIM",Order=7)]
    public List<S_DIM> S_DIM {get; set;}
    [XmlElement("S_DOC",Order=8)]
    public List<S_DOC> S_DOC {get; set;}
    [XmlElement("G_SGP",Order=9)]
    public List<G_SGP> G_SGP {get; set;}
    [XmlElement("G_DGS",Order=10)]
    public List<G_DGS> G_DGS {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_GID {
    [XmlElement(Order=0)]
    public string D_1496_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C213 C_C213 {get; set;}
    [XmlElement(Order=2)]
    public C_C213_2 C_C213_2 {get; set;}
    [XmlElement(Order=3)]
    public C_C213_3 C_C213_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C213 {
    [XmlElement(Order=0)]
    public string D_7224_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7065_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1131_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3055_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_7064_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C213_2 {
    [XmlElement(Order=0)]
    public string D_7224_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7065_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1131_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3055_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_7064_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C213_3 {
    [XmlElement(Order=0)]
    public string D_7224_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7065_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1131_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3055_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_7064_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_HAN {
    [XmlElement(Order=0)]
    public C_C524 C_C524 {get; set;}
    [XmlElement(Order=1)]
    public C_C218 C_C218 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C524 {
    [XmlElement(Order=0)]
    public string D_4079_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4078_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C218 {
    [XmlElement(Order=0)]
    public string D_7419_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_FTX_2 {
    [XmlElement(Order=0)]
    public string D_4451_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_4453_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C107_2 C_C107_2 {get; set;}
    [XmlElement(Order=3)]
    public C_C108_2 C_C108_2 {get; set;}
    [XmlElement(Order=4)]
    public string D_3453_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C107_2 {
    [XmlElement(Order=0)]
    public string D_4441_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C108_2 {
    [XmlElement(Order=0)]
    public string D_4440_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_4440_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_4440_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4440_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_4440_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_RFF_4 {
    [XmlElement(Order=0)]
    public C_C506_4 C_C506_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C506_4 {
    [XmlElement(Order=0)]
    public string D_1153_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1154_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1156_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4000_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_PIA {
    [XmlElement(Order=0)]
    public string D_4347_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C212 C_C212 {get; set;}
    [XmlElement(Order=2)]
    public C_C212_2 C_C212_2 {get; set;}
    [XmlElement(Order=3)]
    public C_C212_3 C_C212_3 {get; set;}
    [XmlElement(Order=4)]
    public C_C212_4 C_C212_4 {get; set;}
    [XmlElement(Order=5)]
    public C_C212_5 C_C212_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C212 {
    [XmlElement(Order=0)]
    public string D_7140_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7143_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1131_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3055_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C212_2 {
    [XmlElement(Order=0)]
    public string D_7140_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7143_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1131_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3055_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C212_3 {
    [XmlElement(Order=0)]
    public string D_7140_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7143_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1131_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3055_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C212_4 {
    [XmlElement(Order=0)]
    public string D_7140_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7143_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1131_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3055_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C212_5 {
    [XmlElement(Order=0)]
    public string D_7140_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7143_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1131_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3055_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_NAD_2 {
    [XmlElement(Order=0)]
    public S_NAD_2 S_NAD_2 {get; set;}
    [XmlElement("S_DTM_3",Order=1)]
    public List<S_DTM_3> S_DTM_3 {get; set;}
    [XmlElement("S_RFF_5",Order=2)]
    public List<S_RFF_5> S_RFF_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_NAD_2 {
    [XmlElement(Order=0)]
    public string D_3035_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C082_2 C_C082_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C058_2 C_C058_2 {get; set;}
    [XmlElement(Order=3)]
    public C_C080_2 C_C080_2 {get; set;}
    [XmlElement(Order=4)]
    public C_C059_2 C_C059_2 {get; set;}
    [XmlElement(Order=5)]
    public string D_3164_6 {get; set;}
    [XmlElement(Order=6)]
    public string D_3229_7 {get; set;}
    [XmlElement(Order=7)]
    public string D_3251_8 {get; set;}
    [XmlElement(Order=8)]
    public string D_3207_9 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C082_2 {
    [XmlElement(Order=0)]
    public string D_3039_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C058_2 {
    [XmlElement(Order=0)]
    public string D_3124_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3124_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3124_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3124_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_3124_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C080_2 {
    [XmlElement(Order=0)]
    public string D_3036_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3036_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3036_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3036_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_3036_5 {get; set;}
    [XmlElement(Order=5)]
    public string D_3045_6 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C059_2 {
    [XmlElement(Order=0)]
    public string D_3042_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3042_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3042_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DTM_3 {
    [XmlElement(Order=0)]
    public C_C507_3 C_C507_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C507_3 {
    [XmlElement(Order=0)]
    public string D_2005_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_2380_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_2379_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_RFF_5 {
    [XmlElement(Order=0)]
    public C_C506_5 C_C506_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C506_5 {
    [XmlElement(Order=0)]
    public string D_1153_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1154_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1156_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4000_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_MEA {
    [XmlElement(Order=0)]
    public string D_6311_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C502 C_C502 {get; set;}
    [XmlElement(Order=2)]
    public C_C174 C_C174 {get; set;}
    [XmlElement(Order=3)]
    public string D_7383_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C502 {
    [XmlElement(Order=0)]
    public string D_6313_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6321_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6155_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6154_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C174 {
    [XmlElement(Order=0)]
    public string D_6411_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6314_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6162_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6152_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_6432_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DIM {
    [XmlElement(Order=0)]
    public string D_6145_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C211 C_C211 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C211 {
    [XmlElement(Order=0)]
    public string D_6411_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6168_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6140_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6008_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DOC {
    [XmlElement(Order=0)]
    public C_C002_2 C_C002_2 {get; set;}
    [XmlElement(Order=1)]
    public C_C503 C_C503 {get; set;}
    [XmlElement(Order=2)]
    public string D_3153_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_1220_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_1218_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C002_2 {
    [XmlElement(Order=0)]
    public string D_1001_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_1000_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C503 {
    [XmlElement(Order=0)]
    public string D_1004_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1373_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1366_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3453_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_SGP {
    [XmlElement(Order=0)]
    public S_SGP S_SGP {get; set;}
    [XmlElement("S_MEA_2",Order=1)]
    public List<S_MEA_2> S_MEA_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_SGP {
    [XmlElement(Order=0)]
    public C_C237 C_C237 {get; set;}
    [XmlElement(Order=1)]
    public string D_7224_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C237 {
    [XmlElement(Order=0)]
    public string D_8260_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3207_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_MEA_2 {
    [XmlElement(Order=0)]
    public string D_6311_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C502_2 C_C502_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C174_2 C_C174_2 {get; set;}
    [XmlElement(Order=3)]
    public string D_7383_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C502_2 {
    [XmlElement(Order=0)]
    public string D_6313_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6321_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6155_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6154_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C174_2 {
    [XmlElement(Order=0)]
    public string D_6411_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6314_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6162_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6152_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_6432_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_DGS {
    [XmlElement(Order=0)]
    public S_DGS S_DGS {get; set;}
    [XmlElement("S_FTX_3",Order=1)]
    public List<S_FTX_3> S_FTX_3 {get; set;}
    [XmlElement("S_MEA_3",Order=2)]
    public List<S_MEA_3> S_MEA_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DGS {
    [XmlElement(Order=0)]
    public string D_8273_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C205 C_C205 {get; set;}
    [XmlElement(Order=2)]
    public C_C234 C_C234 {get; set;}
    [XmlElement(Order=3)]
    public C_C223 C_C223 {get; set;}
    [XmlElement(Order=4)]
    public string D_8339_5 {get; set;}
    [XmlElement(Order=5)]
    public string D_8364_6 {get; set;}
    [XmlElement(Order=6)]
    public string D_8410_7 {get; set;}
    [XmlElement(Order=7)]
    public string D_8126_8 {get; set;}
    [XmlElement(Order=8)]
    public C_C235 C_C235 {get; set;}
    [XmlElement(Order=9)]
    public C_C236 C_C236 {get; set;}
    [XmlElement(Order=10)]
    public string D_8255_11 {get; set;}
    [XmlElement(Order=11)]
    public string D_8325_12 {get; set;}
    [XmlElement(Order=12)]
    public string D_8211_13 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C205 {
    [XmlElement(Order=0)]
    public string D_8351_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8078_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_8092_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C234 {
    [XmlElement(Order=0)]
    public string D_7124_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7088_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C223 {
    [XmlElement(Order=0)]
    public string D_7106_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6411_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C235 {
    [XmlElement(Order=0)]
    public string D_8158_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8186_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C236 {
    [XmlElement(Order=0)]
    public string D_8246_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8246_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_8246_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_FTX_3 {
    [XmlElement(Order=0)]
    public string D_4451_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_4453_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C107_3 C_C107_3 {get; set;}
    [XmlElement(Order=3)]
    public C_C108_3 C_C108_3 {get; set;}
    [XmlElement(Order=4)]
    public string D_3453_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C107_3 {
    [XmlElement(Order=0)]
    public string D_4441_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C108_3 {
    [XmlElement(Order=0)]
    public string D_4440_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_4440_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_4440_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4440_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_4440_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_MEA_3 {
    [XmlElement(Order=0)]
    public string D_6311_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C502_3 C_C502_3 {get; set;}
    [XmlElement(Order=2)]
    public C_C174_3 C_C174_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_7383_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C502_3 {
    [XmlElement(Order=0)]
    public string D_6313_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6321_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6155_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6154_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C174_3 {
    [XmlElement(Order=0)]
    public string D_6411_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6314_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6162_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6152_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_6432_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_EQD {
    [XmlElement(Order=0)]
    public S_EQD S_EQD {get; set;}
    [XmlElement("S_RFF_6",Order=1)]
    public List<S_RFF_6> S_RFF_6 {get; set;}
    [XmlElement(Order=2)]
    public S_EQN S_EQN {get; set;}
    [XmlElement("S_TMD_2",Order=3)]
    public List<S_TMD_2> S_TMD_2 {get; set;}
    [XmlElement("S_DTM_4",Order=4)]
    public List<S_DTM_4> S_DTM_4 {get; set;}
    [XmlElement("S_TSR_2",Order=5)]
    public List<S_TSR_2> S_TSR_2 {get; set;}
    [XmlElement("S_LOC_2",Order=6)]
    public List<S_LOC_2> S_LOC_2 {get; set;}
    [XmlElement("S_MEA_4",Order=7)]
    public List<S_MEA_4> S_MEA_4 {get; set;}
    [XmlElement("S_DIM_2",Order=8)]
    public List<S_DIM_2> S_DIM_2 {get; set;}
    [XmlElement("S_TMP",Order=9)]
    public List<S_TMP> S_TMP {get; set;}
    [XmlElement("S_RNG",Order=10)]
    public List<S_RNG> S_RNG {get; set;}
    [XmlElement("S_SEL",Order=11)]
    public List<S_SEL> S_SEL {get; set;}
    [XmlElement("S_FTX_4",Order=12)]
    public List<S_FTX_4> S_FTX_4 {get; set;}
    [XmlElement(Order=13)]
    public S_DGS_2 S_DGS_2 {get; set;}
    [XmlElement("S_MOA",Order=14)]
    public List<S_MOA> S_MOA {get; set;}
    [XmlElement("S_GOR",Order=15)]
    public List<S_GOR> S_GOR {get; set;}
    [XmlElement(Order=16)]
    public S_EQA S_EQA {get; set;}
    [XmlElement("G_DAM",Order=17)]
    public List<G_DAM> G_DAM {get; set;}
    [XmlElement("G_TDT_2",Order=18)]
    public List<G_TDT_2> G_TDT_2 {get; set;}
    [XmlElement("G_NAD_3",Order=19)]
    public List<G_NAD_3> G_NAD_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_EQD {
    [XmlElement(Order=0)]
    public string D_8053_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C237_2 C_C237_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C224 C_C224 {get; set;}
    [XmlElement(Order=3)]
    public string D_8077_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_8249_5 {get; set;}
    [XmlElement(Order=5)]
    public string D_8169_6 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C237_2 {
    [XmlElement(Order=0)]
    public string D_8260_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3207_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C224 {
    [XmlElement(Order=0)]
    public string D_8155_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_8154_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_RFF_6 {
    [XmlElement(Order=0)]
    public C_C506_6 C_C506_6 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C506_6 {
    [XmlElement(Order=0)]
    public string D_1153_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1154_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_1156_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4000_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_EQN {
    [XmlElement(Order=0)]
    public C_C523 C_C523 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C523 {
    [XmlElement(Order=0)]
    public string D_6350_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6353_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_TMD_2 {
    [XmlElement(Order=0)]
    public C_C219_2 C_C219_2 {get; set;}
    [XmlElement(Order=1)]
    public string D_8332_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_8341_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C219_2 {
    [XmlElement(Order=0)]
    public string D_8335_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8334_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DTM_4 {
    [XmlElement(Order=0)]
    public C_C507_4 C_C507_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C507_4 {
    [XmlElement(Order=0)]
    public string D_2005_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_2380_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_2379_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_TSR_2 {
    [XmlElement(Order=0)]
    public C_C536_2 C_C536_2 {get; set;}
    [XmlElement(Order=1)]
    public C_C233_2 C_C233_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C537_2 C_C537_2 {get; set;}
    [XmlElement(Order=3)]
    public C_C703_2 C_C703_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C536_2 {
    [XmlElement(Order=0)]
    public string D_4065_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C233_2 {
    [XmlElement(Order=0)]
    public string D_7273_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_7273_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_1131_5 {get; set;}
    [XmlElement(Order=5)]
    public string D_3055_6 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C537_2 {
    [XmlElement(Order=0)]
    public string D_4219_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C703_2 {
    [XmlElement(Order=0)]
    public string D_7085_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_LOC_2 {
    [XmlElement(Order=0)]
    public string D_3227_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C517_2 C_C517_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C519_2 C_C519_2 {get; set;}
    [XmlElement(Order=3)]
    public C_C553_2 C_C553_2 {get; set;}
    [XmlElement(Order=4)]
    public string D_5479_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C517_2 {
    [XmlElement(Order=0)]
    public string D_3225_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3224_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C519_2 {
    [XmlElement(Order=0)]
    public string D_3223_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3222_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C553_2 {
    [XmlElement(Order=0)]
    public string D_3233_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3232_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_MEA_4 {
    [XmlElement(Order=0)]
    public string D_6311_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C502_4 C_C502_4 {get; set;}
    [XmlElement(Order=2)]
    public C_C174_4 C_C174_4 {get; set;}
    [XmlElement(Order=3)]
    public string D_7383_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C502_4 {
    [XmlElement(Order=0)]
    public string D_6313_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6321_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6155_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6154_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C174_4 {
    [XmlElement(Order=0)]
    public string D_6411_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6314_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6162_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6152_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_6432_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DIM_2 {
    [XmlElement(Order=0)]
    public string D_6145_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C211_2 C_C211_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C211_2 {
    [XmlElement(Order=0)]
    public string D_6411_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6168_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6140_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6008_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_TMP {
    [XmlElement(Order=0)]
    public string D_6245_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C239 C_C239 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C239 {
    [XmlElement(Order=0)]
    public string D_6246_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6411_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_RNG {
    [XmlElement(Order=0)]
    public string D_6167_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C280 C_C280 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C280 {
    [XmlElement(Order=0)]
    public string D_6411_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6162_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6152_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_SEL {
    [XmlElement(Order=0)]
    public string D_9308_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C215 C_C215 {get; set;}
    [XmlElement(Order=2)]
    public string D_4517_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C215 {
    [XmlElement(Order=0)]
    public string D_9303_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_9302_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_FTX_4 {
    [XmlElement(Order=0)]
    public string D_4451_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_4453_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C107_4 C_C107_4 {get; set;}
    [XmlElement(Order=3)]
    public C_C108_4 C_C108_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_3453_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C107_4 {
    [XmlElement(Order=0)]
    public string D_4441_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C108_4 {
    [XmlElement(Order=0)]
    public string D_4440_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_4440_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_4440_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_4440_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_4440_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DGS_2 {
    [XmlElement(Order=0)]
    public string D_8273_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C205_2 C_C205_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C234_2 C_C234_2 {get; set;}
    [XmlElement(Order=3)]
    public C_C223_2 C_C223_2 {get; set;}
    [XmlElement(Order=4)]
    public string D_8339_5 {get; set;}
    [XmlElement(Order=5)]
    public string D_8364_6 {get; set;}
    [XmlElement(Order=6)]
    public string D_8410_7 {get; set;}
    [XmlElement(Order=7)]
    public string D_8126_8 {get; set;}
    [XmlElement(Order=8)]
    public C_C235_2 C_C235_2 {get; set;}
    [XmlElement(Order=9)]
    public C_C236_2 C_C236_2 {get; set;}
    [XmlElement(Order=10)]
    public string D_8255_11 {get; set;}
    [XmlElement(Order=11)]
    public string D_8325_12 {get; set;}
    [XmlElement(Order=12)]
    public string D_8211_13 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C205_2 {
    [XmlElement(Order=0)]
    public string D_8351_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8078_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_8092_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C234_2 {
    [XmlElement(Order=0)]
    public string D_7124_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_7088_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C223_2 {
    [XmlElement(Order=0)]
    public string D_7106_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6411_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C235_2 {
    [XmlElement(Order=0)]
    public string D_8158_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8186_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C236_2 {
    [XmlElement(Order=0)]
    public string D_8246_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8246_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_8246_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_MOA {
    [XmlElement(Order=0)]
    public C_C516 C_C516 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C516 {
    [XmlElement(Order=0)]
    public string D_5025_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_5004_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6345_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_6343_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_4405_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_GOR {
    [XmlElement(Order=0)]
    public string D_8323_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C232 C_C232 {get; set;}
    [XmlElement(Order=2)]
    public C_C232_2 C_C232_2 {get; set;}
    [XmlElement(Order=3)]
    public C_C232_3 C_C232_3 {get; set;}
    [XmlElement(Order=4)]
    public C_C232_4 C_C232_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C232 {
    [XmlElement(Order=0)]
    public string D_9415_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_9411_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_9417_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_9353_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C232_2 {
    [XmlElement(Order=0)]
    public string D_9415_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_9411_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_9417_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_9353_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C232_3 {
    [XmlElement(Order=0)]
    public string D_9415_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_9411_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_9417_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_9353_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C232_4 {
    [XmlElement(Order=0)]
    public string D_9415_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_9411_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_9417_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_9353_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_EQA {
    [XmlElement(Order=0)]
    public string D_8053_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C237_3 C_C237_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C237_3 {
    [XmlElement(Order=0)]
    public string D_8260_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3207_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_DAM {
    [XmlElement(Order=0)]
    public S_DAM S_DAM {get; set;}
    [XmlElement(Order=1)]
    public S_COD S_COD {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DAM {
    [XmlElement(Order=0)]
    public string D_7493_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C821 C_C821 {get; set;}
    [XmlElement(Order=2)]
    public C_C822 C_C822 {get; set;}
    [XmlElement(Order=3)]
    public C_C825 C_C825 {get; set;}
    [XmlElement(Order=4)]
    public C_C826 C_C826 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C821 {
    [XmlElement(Order=0)]
    public string D_7501_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_7500_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C822 {
    [XmlElement(Order=0)]
    public string D_7503_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_7502_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C825 {
    [XmlElement(Order=0)]
    public string D_7509_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_7508_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C826 {
    [XmlElement(Order=0)]
    public string D_1229_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_1228_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_COD {
    [XmlElement(Order=0)]
    public C_C823 C_C823 {get; set;}
    [XmlElement(Order=1)]
    public C_C824 C_C824 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C823 {
    [XmlElement(Order=0)]
    public string D_7505_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_7504_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C824 {
    [XmlElement(Order=0)]
    public string D_7507_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_7506_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_TDT_2 {
    [XmlElement(Order=0)]
    public S_TDT_2 S_TDT_2 {get; set;}
    [XmlElement("S_LOC_3",Order=1)]
    public List<S_LOC_3> S_LOC_3 {get; set;}
    [XmlElement("S_DTM_5",Order=2)]
    public List<S_DTM_5> S_DTM_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_TDT_2 {
    [XmlElement(Order=0)]
    public string D_8051_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8028_2 {get; set;}
    [XmlElement(Order=2)]
    public C_C220_2 C_C220_2 {get; set;}
    [XmlElement(Order=3)]
    public C_C228_2 C_C228_2 {get; set;}
    [XmlElement(Order=4)]
    public C_C040_2 C_C040_2 {get; set;}
    [XmlElement(Order=5)]
    public string D_8101_6 {get; set;}
    [XmlElement(Order=6)]
    public C_C401_2 C_C401_2 {get; set;}
    [XmlElement(Order=7)]
    public C_C222_2 C_C222_2 {get; set;}
    [XmlElement(Order=8)]
    public string D_8281_9 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C220_2 {
    [XmlElement(Order=0)]
    public string D_8067_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8066_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C228_2 {
    [XmlElement(Order=0)]
    public string D_8179_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8178_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C040_2 {
    [XmlElement(Order=0)]
    public string D_3127_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3128_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C401_2 {
    [XmlElement(Order=0)]
    public string D_8457_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_8459_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_7130_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C222_2 {
    [XmlElement(Order=0)]
    public string D_8213_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_8212_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_8453_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_LOC_3 {
    [XmlElement(Order=0)]
    public string D_3227_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C517_3 C_C517_3 {get; set;}
    [XmlElement(Order=2)]
    public C_C519_3 C_C519_3 {get; set;}
    [XmlElement(Order=3)]
    public C_C553_3 C_C553_3 {get; set;}
    [XmlElement(Order=4)]
    public string D_5479_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C517_3 {
    [XmlElement(Order=0)]
    public string D_3225_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3224_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C519_3 {
    [XmlElement(Order=0)]
    public string D_3223_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3222_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C553_3 {
    [XmlElement(Order=0)]
    public string D_3233_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3232_4 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DTM_5 {
    [XmlElement(Order=0)]
    public C_C507_5 C_C507_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C507_5 {
    [XmlElement(Order=0)]
    public string D_2005_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_2380_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_2379_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class G_NAD_3 {
    [XmlElement(Order=0)]
    public S_NAD_3 S_NAD_3 {get; set;}
    [XmlElement(Order=1)]
    public S_DTM_6 S_DTM_6 {get; set;}
    [XmlElement(Order=2)]
    public S_CTA_2 S_CTA_2 {get; set;}
    [XmlElement(Order=3)]
    public S_COM S_COM {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_NAD_3 {
    [XmlElement(Order=0)]
    public string D_3035_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C082_3 C_C082_3 {get; set;}
    [XmlElement(Order=2)]
    public C_C058_3 C_C058_3 {get; set;}
    [XmlElement(Order=3)]
    public C_C080_3 C_C080_3 {get; set;}
    [XmlElement(Order=4)]
    public C_C059_3 C_C059_3 {get; set;}
    [XmlElement(Order=5)]
    public string D_3164_6 {get; set;}
    [XmlElement(Order=6)]
    public string D_3229_7 {get; set;}
    [XmlElement(Order=7)]
    public string D_3251_8 {get; set;}
    [XmlElement(Order=8)]
    public string D_3207_9 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C082_3 {
    [XmlElement(Order=0)]
    public string D_3039_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_1131_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3055_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C058_3 {
    [XmlElement(Order=0)]
    public string D_3124_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3124_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3124_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3124_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_3124_5 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C080_3 {
    [XmlElement(Order=0)]
    public string D_3036_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3036_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3036_3 {get; set;}
    [XmlElement(Order=3)]
    public string D_3036_4 {get; set;}
    [XmlElement(Order=4)]
    public string D_3036_5 {get; set;}
    [XmlElement(Order=5)]
    public string D_3045_6 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C059_3 {
    [XmlElement(Order=0)]
    public string D_3042_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3042_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_3042_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_DTM_6 {
    [XmlElement(Order=0)]
    public C_C507_6 C_C507_6 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C507_6 {
    [XmlElement(Order=0)]
    public string D_2005_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_2380_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_2379_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_CTA_2 {
    [XmlElement(Order=0)]
    public string D_3139_1 {get; set;}
    [XmlElement(Order=1)]
    public C_C056_2 C_C056_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C056_2 {
    [XmlElement(Order=0)]
    public string D_3413_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3412_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_COM {
    [XmlElement(Order=0)]
    public C_C076 C_C076 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C076 {
    [XmlElement(Order=0)]
    public string D_3148_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_3155_2 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_CNT {
    [XmlElement(Order=0)]
    public C_C270 C_C270 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class C_C270 {
    [XmlElement(Order=0)]
    public string D_6069_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_6066_2 {get; set;}
    [XmlElement(Order=2)]
    public string D_6411_3 {get; set;}
    }
    [Serializable]
    [XmlType(AnonymousType=true, Namespace="www.edifabric.com/edifact")]
    [XmlRoot(Namespace="www.edifabric.com/edifact", IsNullable=false)]
    public class S_UNT {
    [XmlElement(Order=0)]
    public string D_0074_1 {get; set;}
    [XmlElement(Order=1)]
    public string D_0062_2 {get; set;}
    }
}
